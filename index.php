<?php

$url    = $_GET['url'];
$filter = $_GET['filter'];

// Quit if no input parameters
if ( !($url && $filter) ) {
    echo 'No parameters!';
    echo 'Use `url` and `filter` GET parameters!';
    die();
}

// Sanitize user inputs
//$url = escapeshellcmd($url);
//$url = escapeshellarg($url);

// Fetch XML
$ch     = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$xml    = curl_exec($ch);
curl_close($ch);

// Quit if failed
if ( $xml === false ) {
    echo "Error occurred!";
    echo '$xml === false';
    die();
}

// Parse and filter with SimpleXML
$rss    = new SimpleXMLElement($xml);
$filtereditems = [];

// Find and collect
foreach($rss->channel->item as $item) {
    if ( strpos($item->title, $filter) !== false ) {
        array_push($filtereditems, $item->asXML());
    }
}

// Stop if none found
if ( empty($filtereditems) ) {
    echo 'No items with $filter found!';
    die();
}

// Build Header and Footer
$header = $rss->asXML();
$endpos = strpos($header, '<item>');
$header = substr($header, 0, $endpos);

$footer = "\r\n\r\n</channel>\r\n</rss>";

// Output
echo $header;
foreach( $filtereditems as $item) {
    echo $item;
}
echo $footer;

?>
